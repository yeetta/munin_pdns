# README #

A collection of simple plugins for [Munin](http://munin-monitoring.org/) to monitor [PowerDNS](http://powerdns.com/)

Drop the plugins in your $MUNIN_HOME/plugins folder and restart the munin-node process.
